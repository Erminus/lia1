import { Component, OnInit,ElementRef,ViewChild} from '@angular/core';
import {User} from '../user'
import { AngularFire, FirebaseListObservable, FirebaseObjectObservable} from 'angularfire2'; // Angular FirebaseListObservable
import {TranslateService} from 'ng2-translate/ng2-translate'; // Translate service

import {FILE_UPLOAD_DIRECTIVES, FileUploader} from 'ng2-file-upload/ng2-file-upload';





@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})


 
export class LoginFormComponent implements OnInit {
  // An instance of User
    users = new User;
    test:any;

  // Creating Firebase
  items: FirebaseListObservable<any[]>;
  // Image upload
  image:any; // image from db
  imageTest:any; // Upload image
                          file_src :string[] = [];  /// TODO
  // Uuid 
  private uuidImage:string;

  photo:any; // Image object
  images:string;

  constructor(
    private _af: AngularFire, 
    private translate:TranslateService,
    private element: ElementRef) {
      this.items = _af.database.list('person/user'); // Getting data from database
      translate.addLangs(["en","se","es"]); // Adding language to TranslateService
      translate.setDefaultLang('en');

    // Getting browser language and adding default language
          let browserLang = translate.getBrowserLang();
        translate.use(browserLang.match(/en|se|es/) ? browserLang : 'en');


 
  }
   changeListner(event) {
     var reader:any;
        reader = new FileReader();
        var imag = this.element.nativeElement.querySelector('.imag');

        reader.onload = function(e) {
            var src = e.target.result;
            imag.src = src;
        };

        reader.readAsDataURL(event.target.files[0]);
        console.log(imag)
    }
 
    
  // Upload to firebase storage
  handleInputImage($event){

    // this.photo = (<HTMLInputElement>document.getElementById('file')).files[0];

    // var reader:any;
    // reader = new FileReader();
    

    // reader.onload = function(e){
    //   this.images = e.target.result;
    //   console.log(this.images);
    // }
    // reader.readAsDataURL(this.photo);
  
    

    

    // transform image to file
    //this.photo= $event.dataTransfer ? $event.dataTrasfer.files[0] : $event.target.files[0];
    // making check if image
    var pattern = /image-*/;
    // Checling if file is type of valid image
    if (!this.photo.type.match(pattern)) {
      alert('invalid format')
      return;
    }else{
              // Unique id
    this.uuidImage = this.generateUniqueID();
    this.users.imageRef = this.uuidImage;
    // *************************************************Maybe save to local variable********************
    
    // var img  = document.createElement('img');
    // img.src = window.URL.createObjectURL(this.photo);

    // Create a File Reader
   
    //   var reader:any,
    //   target:EventTarget;
    //   reader= new FileReader();
    //   reader.onload = function (imgsrc:any){
    //     var fileUrl = imgsrc.target.result;
    //     console.log(fileUrl)
    //   }
    // reader.readAsDataURL();
   //console.log(this.photo )
    //this.addImageToDb() // adding to database
    //this.getImageFromDB(this.users.imageRef);
    }
  }

  // // Conver and save image file to url
  // saveImageFileToUrl(inputValue:any):void{

    
  // }
  // Getting image from db
  getImageFromDB(fileUrl:string){
                        // Getting image from firebase starge
      const storageRef = firebase.storage().ref().child('images/'+this.uuidImage);
      storageRef.getDownloadURL().then(url => this.imageTest = url); 
  }

  addImageToDb(){
    var storageRef = firebase.storage().ref();
    var mountainsRef = storageRef.child('images/'+this.users.imageRef);
      mountainsRef.put(this.photo).then(function(snapshot) {
        console.log('Uploaded a blob or file!');
      });
  }

  // generate unique image id
generateUniqueID():string{
function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
    s4() + '-' + s4() + s4() + s4();
}



  // On init method
  ngOnInit() {

  }


// Printing out user info from firebase
  userInfo(){
         // Adding file to the database
    this.addImageToDb();
    // Pushing all dat to db
    this.items.push({
      email:this.users.email, 
      firstName:this.users.firstName, 
      description:this.users.description,
      imageReference:this.users.imageRef,
    image: this.photo
  });
  }


}
