import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule,Http} from '@angular/http';
import { AppComponent } from './app.component';
import { LoginFormComponent } from './login-form/login-form.component';
import { AngularFireModule,AngularFire} from 'angularfire2';
import {TranslateModule} from 'ng2-translate/ng2-translate';
import { UPLOAD_DIRECTIVES} from 'ng2-uploader/ng2-uploader';
import {FILE_UPLOAD_DIRECTIVES, FileUploader} from 'ng2-file-upload/ng2-file-upload';
//import * as firebase from "firebase";





// Must export the config
export const firebaseConfig = {
    apiKey: "AIzaSyAhUQgQUqPOhqMUyLAkOmLz5Onf94GuUTw",
    authDomain: "rdcwebb.firebaseapp.com",
    databaseURL: "https://rdcwebb.firebaseio.com",
    storageBucket: "rdcwebb.appspot.com"
};
declare var firebase:any;
declare var result:any;

@NgModule({
  declarations: [
    AppComponent,
    LoginFormComponent,
    UPLOAD_DIRECTIVES,
    FILE_UPLOAD_DIRECTIVES
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AngularFireModule.initializeApp(firebaseConfig),
    TranslateModule.forRoot()
  ],
  providers: [AngularFire],
  bootstrap: [AppComponent]
})
export class AppModule { }
